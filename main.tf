terraform {
  required_version = ">= 0.12.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.6.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "2.2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "1.3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.2"
    }
  }

  # Descomente e altere o nome do bucket 
  # para salvar o tfstate no S3
  # backend "s3" {
  #   bucket = "change_me"
  #   key    = "tfstate"
  #   region = "us-east-1"
  # }
}

provider "aws" {
  profile = var.profile
  region  = var.region
}

provider "helm" {
  kubernetes {
    host                   = aws_eks_cluster.eks.endpoint
    cluster_ca_certificate = base64decode(aws_eks_cluster.eks.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.eks.token
    load_config_file       = false
  }
}

provider "kubernetes" {
  host                   = aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
  load_config_file       = false
}

# Consulta realizada somente após criação do cluster
data "aws_eks_cluster_auth" "eks" {
  name = aws_eks_cluster.eks.id
}