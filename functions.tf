
# Resource's names
locals {
  vpc_name         = "${var.prefix}-vpc-${var.environ}"
  subnet_name      = "${var.prefix}-subnet-${var.environ}"
  igw_name         = "${var.prefix}-igw-${var.environ}"
  cluster_name     = "${var.prefix}-cluster-${var.environ}"
  iam_cluster_name = "${var.prefix}-iam-role-master-${var.environ}"
  key_pair_name    = "${var.prefix}-key-pair-${var.environ}"
  node_group_name  = "${var.prefix}-node-group-${var.environ}"
  iam_workers_name = "${var.prefix}-iam-role-workers-${var.environ}"
  sg_cluster_name  = "${var.prefix}-cluster-sg-${var.environ}"
  sg_workers_name  = "${var.prefix}-workers-sg-${var.environ}"
}


# Network functions
locals {
  count_subnets = length(var.subnet_cidr_block)
}
