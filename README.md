# Cluster k8s com serviço de monitoramento de rede

![varios_nodes](img/varios_nodes.png)

## PRE-REQ

- terraform
- awscli
- kubectl
- bucket S3

## Setup

__Instalando `awscli`__:

__Linux__

Baixe o CLI:
```
$ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
```
Descompacte o arquivo baixado: 
``` bash
$ unzip awscliv2.zip
```
Execute o script de instalação:
``` bash
$ sudo ./aws/install
```

Instalar [outras versões](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-cliv2.html) veja na página oficial. 

__Configurando `aws`__:

Configure suas credenciais na aws. Essa configuração será usada pelo terraform.
``` bash
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-east-1 
Default output format [None]: 
```

__Instalando `kubectl`__:

__Linux:__

Baixe CLI:
``` bash
$ curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
```
Torne o binário executável:
``` bash
$ chmod +x ./kubectl
```
Mova para o diretório PATH do sistema:
``` bash
$ sudo mv ./kubectl /usr/local/bin/kubectl
```
Valide a instalação:
``` bash
$ kubectl version --client
```
Ao final, criar o diretório default do `kubectl`:
``` bash
$ mkdir ~/.kube
``` 

Instalar [outras versões](https://kubernetes.io/docs/tasks/tools/install-kubectl/) veja na página oficial.


## Executando `terraform`

A configuração do backend está comentada. O arquivo `tfstate` ficará local no diretório do projeto. Caso queira enviar o `tfstate` para o S3, descomente o trecho abaixo no arquivo `main.tf` e passe o nome do bucket correspondente.

``` go 
  backend "s3" {
    bucket = "change_me"
    key    = "tfstate"
    region = "us-east-1"
  }
```

Atenção: não deixe de configurar o awscli com as credenciais de acesso. O arquivo gerado pelo awscli (`~/.aws/credentials`) é utilizado pelo terraform para conectar na aws e atualizar o `kubectl` com as credenciais do cluster provisionado.

__Inicializando__:
``` bash
$ terraform init
```
__Gerando plano__:

``` bash
$ terraform plan -var-file="homolog.tfvars" -out=plan.out
```
__Aplicando terraform__:

``` bash
$ terraform apply plan.out
```

## Testando `Golpinger`

Execute o __port-forward__ para o services `Golpinger`, `Prometheus` e `Grafana`:

As credenciais de acesso do grafana são `admin/admin`.

``` bash
kubectl port-forward svc/prometheus-server 9090 &
kubectl port-forward svc/grafana 3000 &
kubectl port-forward svc/goldpinger 8888 &
```

### Consideração

* A vpc foi configurada com subnets [somente em rede pública](https://docs.aws.amazon.com/pt_br/vpc/latest/userguide/VPC_Scenario1.html).

* Não configurei um ingress para acessar grafana, prometheus, goldpinger, por isso a orientação de fazer o `port_forward`.
