
# cluster
environ = "homolog"
prefix = "myeks"
cluster_version = "1.17"

# node group 
instance_types = ["t2.small"]
desired_size = 3
max_size = 3
min_size = 1

# network
cidr_block = "10.0.0.0/16"
availability_zone = ["us-east-1a", "us-east-1b"]
subnet_cidr_block = ["10.0.0.0/20", "10.0.16.0/20"]

# aws
region = "us-east-1"
profile = "default"