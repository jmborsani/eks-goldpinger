
resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "aws_key_pair" "ec2_key_pair" {
  key_name   = local.key_pair_name
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "aws_eks_node_group" "workers" {
  cluster_name    = local.cluster_name
  node_group_name = local.node_group_name
  node_role_arn   = aws_iam_role.workers.arn
  instance_types  = var.instance_types
  version         = var.cluster_version
  subnet_ids      = aws_subnet.eks_subnets.*.id

  # Atenção: o security-group criado está liberando acesso ssh para 10.0.0.0/16
  remote_access {
    ec2_ssh_key               = aws_key_pair.ec2_key_pair.key_name
    source_security_group_ids = list(aws_security_group.workers.id)
  }

  scaling_config {
    desired_size = var.desired_size
    max_size     = var.max_size
    min_size     = var.min_size
  }

  depends_on = [
    aws_iam_role_policy_attachment.workers-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.workers-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.workers-AmazonEC2ContainerRegistryReadOnly,
    aws_eks_cluster.eks,
    aws_security_group.workers,
    aws_security_group.cluster,
  ]

  # Terraform ignorar o AutoScaling externo
  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }
}

resource "local_file" "ssh-key" {
  filename        = pathexpand("~/.ssh/eks_cluster.pem")
  file_permission = "0600"
  content         = tls_private_key.rsa.private_key_pem
}

resource "aws_iam_role" "workers" {
  name = local.iam_workers_name

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "workers-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.workers.name
}
