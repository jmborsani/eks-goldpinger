
data "aws_security_groups" "vpc_main" {
  filter {
    name   = "vpc-id"
    values = ["${aws_vpc.main.id}"]
  }

  filter {
    name   = "group-name"
    values = ["default"]
  }
}

resource "aws_security_group" "workers" {
  name        = local.sg_workers_name
  description = "Securty group applied to workers nodes"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = local.sg_workers_name
  }
}

resource "aws_security_group" "cluster" {
  name        = local.sg_cluster_name
  description = "Security group applied to cluster"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = local.sg_cluster_name
  }
}
