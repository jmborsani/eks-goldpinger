
resource "aws_eks_cluster" "eks" {
  name     = local.cluster_name
  role_arn = aws_iam_role.cluster.arn
  version  = var.cluster_version

  vpc_config {
    subnet_ids         = aws_subnet.eks_subnets.*.id
    security_group_ids = list(aws_security_group.cluster.id)
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster-AmazonEKSClusterPolicy,
    aws_vpc.main,
    aws_subnet.eks_subnets,
    aws_internet_gateway.eks_igw,
    aws_route.associate_igw_vpc,
    aws_security_group.cluster,
    aws_security_group.workers,
  ]

  # Cria dependencia de ter o cli aws instalado
  # Porem nao cria a necessidade de implementar depends_on  
  provisioner "local-exec" {
    command = "aws eks --profile ${var.profile} --region ${var.region} update-kubeconfig --name ${local.cluster_name} --alias ${local.cluster_name}"
  }
}

resource "aws_iam_role" "cluster" {
  name = local.iam_cluster_name

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster.name
}

