resource "aws_vpc" "main" {
  cidr_block           = var.cidr_block
  enable_dns_hostnames = true

  tags = {
    Name = "${local.vpc_name}"
  }

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

# 2 pub subnets (in two az's) 
resource "aws_subnet" "eks_subnets" {
  count = local.count_subnets

  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet_cidr_block[count.index]
  availability_zone       = element(var.availability_zone, count.index % local.count_subnets)
  map_public_ip_on_launch = true

  tags = {
    Name                     = "${local.subnet_name}-${count.index}"
    "kubernetes.io/role/elb" = 1
  }

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

# add internet gateway (public acess)
resource "aws_internet_gateway" "eks_igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${local.igw_name}"
  }
}

data "aws_route_table" "selected" {
  vpc_id = aws_vpc.main.id
}

# associate igw to route-table
resource "aws_route" "associate_igw_vpc" {
  route_table_id         = data.aws_route_table.selected.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.eks_igw.id
}
