
variable "environ" {
  type    = string
  default = "homolog"
}

variable "prefix" {
  type    = string
  default = "xpto"
}

variable "cluster_version" {
  type    = string
  default = "1.17"
}

variable "profile" {
  type    = string
  default = "default"
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "instance_types" {
  type    = list(string)
  default = ["t2.small"]
}

variable "desired_size" {
  type    = number
  default = 2
}

variable "max_size" {
  type    = number
  default = 3
}

variable "min_size" {
  type    = number
  default = 1
}

variable "cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "availability_zone" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b"]
}

variable "subnet_cidr_block" {
  type    = list(string)
  default = ["10.0.0.0/20", "10.0.16.0/20"]
}
