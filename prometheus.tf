resource "helm_release" "prometheus" {
  name            = "prometheus"
  chart           = "./charts/prometheus-11.15.0.tgz"
  wait            = true
  timeout         = 600
  force_update    = true
  recreate_pods   = true
  cleanup_on_fail = true

  set {
    name  = "server.service.servicePort"
    value = "9090"
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}
