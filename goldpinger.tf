resource "kubernetes_service_account" "goldpinger" {
  metadata {
    name = "goldpinger-serviceaccount"
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}

resource "kubernetes_cluster_role" "goldpinger" {
  metadata {
    name = "goldpinger-clusterrole"
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["list", "get", "watch"]
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}

resource "kubernetes_cluster_role_binding" "goldpinger" {
  metadata {
    name = "goldpinger-clusterrolebinding"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "goldpinger-clusterrole"
  }

  subject {
    kind = "ServiceAccount"
    name = "goldpinger-serviceaccount"
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}

resource "kubernetes_daemonset" "goldpinger" {
  metadata {
    name = "goldpinger"
    labels = {
      app     = "goldpinger"
      version = "v3.0.0"
    }
  }

  spec {
    strategy {
      type = "RollingUpdate"
    }
    selector {
      match_labels = {
        app = "goldpinger"
      }
    }
    template {
      metadata {
        labels = {
          app     = "goldpinger"
          version = "v3.0.0"
        }
      }

      spec {
        automount_service_account_token = true
        service_account_name            = "goldpinger-serviceaccount"

        security_context {
          run_as_non_root = true
          run_as_user     = 1000
          fs_group        = 2000
        }

        container {
          name = "goldpinger"

          env {
            name  = "HOST"
            value = "0.0.0.0"
          }
          env {
            name  = "PORT"
            value = "8888"
          }
          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                field_path = "spec.nodeName"
              }
            }
          }
          env {
            name = "POD_IP"
            value_from {
              field_ref {
                field_path = "status.podIP"
              }
            }
          }

          image             = "docker.io/bloomberg/goldpinger:v3.0.0"
          image_pull_policy = "Always"

          security_context {
            allow_privilege_escalation = false
            read_only_root_filesystem  = true
          }

          resources {
            limits {
              memory = "80Mi"
            }
            requests {
              cpu    = "1m"
              memory = "40Mi"
            }
          }

          port {
            container_port = 8888
            name           = "http"
          }

          readiness_probe {
            http_get {
              path = "/healthz"
              port = 8888
            }
            initial_delay_seconds = 20
            period_seconds        = 5
          }

          liveness_probe {
            http_get {
              path = "/healthz"
              port = 8888
            }
            initial_delay_seconds = 20
            period_seconds        = 5
          }
        }
      }
    }
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}

resource "kubernetes_service" "goldpinger" {
  metadata {
    name = "goldpinger"
    labels = {
      app = "goldpinger"
    }
    annotations = {
      "prometheus.io/scrape" = "true"
      "prometheus.io/path"   = "/metrics"
      "prometheus.io/port"   = "8888"
    }
  }

  spec {
    selector = {
      app = "goldpinger"
    }


    port {
      port      = 8888
      node_port = 30080
      name      = "http"
    }

    type = "NodePort"
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}
