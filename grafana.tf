
resource "kubernetes_config_map" "dashboard" {
  metadata {
    name = "goldpinger-grafana-dashboard"
    labels = {
      grafana_dashboard = "1"
    }
  }

  data = {
    "goldpinger-dashboard.json" = "${file("files/goldpinger.json")}"
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}

resource "kubernetes_secret" "datasource" {
  metadata {
    name = "goldpinger-grafana-datasource"
    labels = {
      "grafana_datasource" = "1"
    }
  }

  type = "Opaque"

  data = {
    "datasource.yaml" = "${file("files/datasource.yaml")}"
  }

  depends_on = [
    aws_eks_node_group.workers,
  ]
}

resource "helm_release" "grafana" {
  name            = "grafana"
  chart           = "./charts/grafana-5.6.11.tgz"
  force_update    = true
  recreate_pods   = true
  cleanup_on_fail = true

  set {
    name  = "service.port"
    value = "3000"
  }

  set {
    name  = "adminUser"
    value = "admin"
  }

  set {
    name  = "adminPassword"
    value = "admin"
  }

  set {
    name  = "sidecar.dashboards.enabled"
    value = "true"
  }

  set {
    name  = "sidecar.datasources.enabled"
    value = "true"
  }

  depends_on = [
    kubernetes_config_map.dashboard,
    kubernetes_secret.datasource,
  ]
}
